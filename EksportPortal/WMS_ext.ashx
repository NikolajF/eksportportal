﻿<%@ WebHandler Language="C#" Class="WMS" %>

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.SessionState;

//using LE34.Data.Access;

//using ai;

/// <summary>
/// A proxy class to proxy HTTP calls to a local WMS server installation, e.g. GeoServer
/// </summary>
public class WMS : IHttpHandler, IRequiresSessionState
{
    /// <summary>
    /// Implements the IHttpHandler.ProcessRequest
    /// WMSServerUrl configured in the web.config file is used as the destination host url
    /// </summary>
    /// <param name="context"></param>
    public void ProcessRequest(HttpContext context)
    {                
        // her kommer vi kun til egne lag
        string WMSServerUrl = ConfigurationManager.AppSettings["WMSServerUrl"];
        string url = WMSServerUrl + context.Request.Url.Query;
        ProxyService.ProxyRequest(ref context, url, null);
    }
    
    /// <summary>
    /// Implements IHttpHandler.IsReusable
    /// Returns false
    /// </summary>
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}