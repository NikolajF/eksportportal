﻿/*
https://www.google.dk/maps/dir/56.7014899,9.5910641/55.2759863,9.3054195/data=!3m1!4b1!4m2!4m1!3e0

 * ruteplan fra google
 * https://www.google.dk/maps/dir/fra_lon,fra_lat/til_lon,til_lat/data=!3m1!4b1!4m2!4m1!3e0
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Configuration;
//using LE34.Data.Access;
using System.Xml;
using System.Net;
using System.IO;

namespace EksportPortal
{
	/// <summary>
	/// Summary description for EksportPortal
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	// [System.Web.Script.Services.ScriptService]
	public class EksportPortal : System.Web.Services.WebService
	{
		XmlElement kd = null;
		XmlDocument doc = null;
		private void startXML(string navn)
		{
			doc = new XmlDocument();

			//(1) the xml declaration is recommended, but not mandatory
			XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
			XmlElement root = doc.DocumentElement;
			doc.InsertBefore(xmlDeclaration, root);

			//(2) string.Empty makes cleaner code
			kd = doc.CreateElement(string.Empty, navn, string.Empty);
			doc.AppendChild(kd);
			//return doc;
		}

		// fasthold de gamle navne
		[WebMethod]
		public XmlDocument chr_webservice(string chr_nr)
		{
			// kan kaldes med et enkelt eller flere chr-numre, med forskellige svar 
			startXML("Kortdata");
			if (chr_nr.Contains(","))
			{
				// flere chr, find kort-koordinater
				string sqlstr = "SELECT min(stald_x_koor) as xmin, min(stald_y_koor) as ymin, max(stald_x_koor) as xmax, max(stald_y_koor) as ymax FROM chr.chr_samlet WHERE chr_nr in(" + chr_nr + ")";
				DataRow rk = new util().db_enkeltrække(sqlstr);
				if (rk == null)
				{
					skrivFejl("Ingen gyldige CHR-numre");
					return doc;
				}
				else
				{
					// konstanter - kortets mindste tilladte størrelse i meter
					double xmin = 500;
					double ymin = 500;
					// konstanter - kortets faktiske størrelse i pixel
					double bredde = 500;
					double hoejde = 500;

					double x1 = rk.Field<double>(0);
					double y1 = rk.Field<double>(1);
					double x2 = rk.Field<double>(2);
					double y2 = rk.Field<double>(3);
					double xdif = x2 - x1;
					double ydif = y2 - y1;


					//der er behov for at gøre plads til de røde prikker på alle fire sider =15px og desuden skal der være plads til label til højre = 70px extra
					//dvs at midt-x skal flyttes 35px til højre og at der så skal lægges 50px til radius-x og 15px til radius-y

					double xret = xdif / (bredde - 100); // den enkelte enhed, fordi det skal bruges på flere måder
					//		echo "<xret>".$xret."</xret>";	
					//echo "<bredde>".($bredde -90)."</bredde>";	
					x1 -= xret * 15;
					x2 += xret * 85;
					xdif = x2 - x1;

					double yret = ydif / (hoejde - 30); // den enkelte enhed
					y1 -= yret * 15;
					y2 += yret * 15;
					ydif = y2 - y1;

					double xcenter = (x1 + x2) / 2;
					double ycenter = (y1 + y2) / 2;
					// dernæst skal det tjekkes at kortet er over evt minimumszoom
					if (xdif < xmin) xdif = xmin;
					if (ydif < ymin) ydif = ymin;
					//	og endelig skal radius tilpassses kortets ønskedes størrelse
					// hvilken af værdierne er størst i forhold til det ønskede størrelsesforhold ?


					if ((xdif / bredde) > (ydif / hoejde))
						ydif = xdif * (hoejde / bredde);
					else
						xdif = ydif * (bredde / hoejde);

					double xradius = xdif / 2;
					double yradius = ydif / 2;
					//echo "<xcenter>".$xcenter."</xcenter>";	
					//echo "<xradius>".$xradius."</xradius>";	
					string mapxt = "&xmin=" + (xcenter - xradius) + "&ymin=" + (ycenter - yradius) + "&xmax=" + (xcenter + xradius) + "&ymax=" + (ycenter + yradius);
					string srv = HttpContext.Current.Request.Url.ToString();
					srv = srv.Substring(0, srv.IndexOf(HttpContext.Current.Request.Url.AbsolutePath));
					addXML(ref kd, "KortUrl", srv + "/kort.ashx?kort=detail&chr_nr=" + chr_nr + mapxt);
					addXML(ref kd, "Oversigtskort", srv + "/kort.ashx?kort=oversigt" + mapxt);
					return doc;


					//echo "<KortUrl>".htmlentities("http://webgis-a.le34.dk/cgi-bin/mapserv.exe?mode=map&map=c:/www/html/intragis/1/webm_mapfile.map&mapext=".($xcenter-$xradius)."+".($ycenter-$yradius)."+".($xcenter + $xradius)."+".($ycenter + $yradius)."&mapsize=".$bredde."+".$hoejde."&layers=KMS_skaermkort+Husnumre+chrnr&MAP_IMAGETYPE=jpeg&chrnummer=".$chr_nr."&map_transparent=off")."</KortUrl>";	
					//// og så et oversigtskort
					//echo "<Oversigtskort>".htmlentities("http://webgis-a.le34.dk/cgi-bin/mapserv.exe?mode=reference&map=c:/www/html/intragis/1/webm_mapfile.map&mapext=".($xcenter-$xradius)."+".($ycenter-$yradius)."+".($xcenter + $xradius)."+".($ycenter + $yradius)."&mapsize=".$bredde."+".$hoejde)."</Oversigtskort>";	
					//echo "</Kortdata>";	
				}

			}
			else // enkelt chr, find oplysninger
			{
				string sqlstr = @"SELECT distinct a.stald_x_koor, a.stald_y_koor, k.komnavn, b.brsregion, pl.kontroldistriktnavn, p.polkr_navn, s.skovdistrikt,st_x(st_transform(a.geom, 4326)) as lon, st_y(st_transform(a.geom, 4326)) as lat 
					FROM chr.chr_samlet a 
					left join dagi.dagi_m10_kommune k on st_within(a.geom, k.geom) 
					left join dagi.brs_regioner b on st_within(a.geom, b.geom)
					left join dagi.plantedistrikter_2009 pl on st_within(a.geom, pl.geom)
					left join dagi.dagi_10_politikr p on st_within(a.geom, p.geom)
					left join dagi.skovlov_borgerindgang_til_fvm s  on st_within(a.geom, s.geom)
					WHERE a.chr_nr =" + chr_nr;
				DataRow rk = new util().db_enkeltrække(sqlstr);
				if (rk == null)
				{
					skrivFejl("CHR findes ikke");
					return doc;
				}
				else if (rk[0].ToString() == "")
				{
					skrivFejl("CHR har ingen staldkoordinater");
					return doc;
				}
				else
				{
					double x = rk.Field<double>(0);
					double y = rk.Field<double>(1);
					string mapxt = "&xmin=" + (x - 1000) + "&ymin=" + (y - 1000) + "&xmax=" + (x + 1000) + "&ymax=" + (y + 1000);
					string srv = HttpContext.Current.Request.Url.ToString();
					srv = srv.Substring(0, srv.IndexOf(HttpContext.Current.Request.Url.AbsolutePath));
					addXML(ref kd, "KortUrl", srv + "/kort.ashx?kort=detail&chr_nr=" + chr_nr + mapxt);
					mapxt = "&xmin=" + (x - 100) + "&ymin=" + (y - 100) + "&xmax=" + (x + 100) + "&ymax=" + (y + 100);
					addXML(ref kd, "Luftfoto", srv + "/kort.ashx?kort=luft&chr_nr=" + chr_nr + mapxt);
					mapxt = "&xmin=" + (x - 500) + "&ymin=" + (y - 500) + "&xmax=" + (x + 500) + "&ymax=" + (y + 500);
					addXML(ref kd, "Oversigtskort", srv + "/kort.ashx?kort=oversigt" + mapxt);

					addXML(ref kd, "Kommune", rk.Field<string>("komnavn"));
					addXML(ref kd, "Beredskabsstyrelsen", rk.Field<string>("brsregion"));
					addXML(ref kd, "Politikreds", rk.Field<string>("polkr_navn"));
					addXML(ref kd, "SNS", rk.Field<string>("skovdistrikt"));
					addXML(ref kd, "Plantedirektoratet", rk.Field<string>("kontroldistriktnavn"));

					addXML(ref kd, "lat", rk["lat"].ToString());
					addXML(ref kd, "lon", rk["lon"].ToString());
				}
			}
			return doc;
		}

		[WebMethod]
		public XmlDocument chr_adresse_ws(string vejnavn ="", string husnr ="", string postnr ="")
		{
			startXML("Adressedata");
			vejnavn= vejnavn.Trim();
			vejnavn= vejnavn.Replace("%20", " ");

			husnr = husnr.ToUpper().Replace("%20", "");
			if (husnr.ToLower() == husnr)
				husnr +="*"; // søg på alle bogstaver

			// kontroller at alle inputparametre er korrekt udfyldt
			if (vejnavn=="" || husnr=="" || postnr=="")	
			{
				skrivFejl("Både vejnavn, husnr og postnr skal udfyldes");	
				return doc;
			}

			// kontroller at postnummeret er et firecifret tal
			int post_tal =0;
			int.TryParse(postnr,out post_tal);
			if (post_tal < 1000 || post_tal > 9999)
			{
				skrivFejl("Postnr skal udfyldes med et firecifret tal");	
				return doc;
			}
			double x = 0;
			double y = 0;
			// kald til Kortforsyningen
			HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create("http://kortforsyningen.kms.dk/?servicename=RestGeokeys_v2&method=adresse&f=xml&vejnavn=" + vejnavn + "&husnr=" + husnr + "&postnr=" + post_tal.ToString() + "&hits=50&geometry=true&login=" + ConfigurationManager.AppSettings["KMSlogin"] + "&password=" + ConfigurationManager.AppSettings["KMSpassword"]);
			Stream res = req.GetResponse().GetResponseStream();
			StreamReader reader = new StreamReader(res);
			string ksvar = reader.ReadToEnd();
			util ut = new util();
			try
			{
				XmlDocument xml = new XmlDocument();
				xml.LoadXml(ksvar);
				 XmlNodeList featList = xml.GetElementsByTagName("Feature");
				// antal
				int antal = featList.Count;
				addXML(ref kd, "Antal", antal.ToString());
				int a = 0;
				if (antal == 0 )
				{
					skrivFejl(">Adressen er ikke gyldig");
					return doc;
				}
				else
				{
					for (int i =0; i < antal; i++)
					{
						XmlElement ad = doc.CreateElement(string.Empty, "Adresse", string.Empty);
						string vej =featList[i].SelectSingleNode("properties/vej/navn").InnerText;
						addXML(ref ad, "Vejnavn", vej);
						string hu=featList[i].SelectSingleNode("properties/husnr").InnerText;
						addXML(ref ad, "Husnr", hu);
						string po =featList[i].SelectSingleNode("properties/postdistrikt/kode").InnerText;
						addXML(ref ad, "Postnr", po);
						addXML(ref ad, "Postdistrikt", featList[i].SelectSingleNode("properties/postdistrikt/navn").InnerText);
						// kortforsyningen giver ikke supllerende stednavn
						// addXML(ref ad, "Supplerende_stednavn", featList[i].SelectSingleNode("properties/postdistrikt/navn").InnerText);
						addXML(ref ad, "Kommunenavn", featList[i].SelectSingleNode("properties/kommune/navn").InnerText);
						string[] geo = featList[i].SelectSingleNode("geometry").InnerText.Replace("POINT(", "").Replace(")", "").Split(' ');
						if (x==0)
						{
							x = double.Parse(geo[0]);
							y = double.Parse(geo[1]);
						}
						addXML(ref ad, "Northing", geo[1]);
						addXML(ref ad, "Easting", geo[0]);
						// find chr ud fra adresse
						// hvis der er et bogstav i husnr, skal der et mellemrum ind
						foreach(char c in hu)
						{
							if (Char.IsLetter(c)) 
							{
								hu = hu.Substring(0,hu.Length-1) + " " + hu.Substring(hu.Length-1,1);
								break;
							}
						}
						string chr = ut.db_enkeltopslag("select chr_nr from chr.chr_samlet where vej ='" + vej + " " + hu +"' and postnr=" + postnr + " limit 1");
						if (chr == "0") chr = "";
						addXML(ref kd, "chr_nummer", chr);
						a++;
					}
					addXML(ref kd, "faktisk_antal", a.ToString());
					// find veterinærsektion
					string vet = ut.db_enkeltopslag("select veterinaersektion from dagi.veterinaersektioner_webgis_2012 where st_within(st_GeomFromText('POINT(" + x + " " + y + ")',25832),geom) limit 1");
					if (vet == "0") vet = "";
					addXML(ref kd, "Veterinærsektion", vet);

					// find chr i nærheden
					DataBase db = ut.db_query("select distinct chr_nr, st_distance(geom, st_GeomFromText('POINT(" + x + " " + y + ")',25832)) as dist, vej, bynavn, postnr, postby, stald_x_koor, stald_y_koor,st_x(st_transform(geom, 4326)) as lon, st_y(st_transform(geom, 4326)) as lat  from chr.chr_samlet where st_distance(geom, st_GeomFromText('POINT(" + x + " " + y + ")',25832)) <1000 order by 2");
					XmlElement cr = doc.CreateElement(string.Empty, "chr_indenfor_radius", string.Empty);
					if (db.Result.Rows.Count > 0)
					{
						foreach (DataRow rk in db.Result.Rows)
						{
							XmlElement b = doc.CreateElement(string.Empty, "Besætning", string.Empty);
							addXML(ref b, "afstand", Math.Round(rk.Field<double>("dist"), 0).ToString() + " m");
							addXML(ref b, "chr_nummer", rk["chr_nr"].ToString());
							addXML(ref b, "adresse", rk["vej"].ToString());
							addXML(ref b, "supplerende_stednavn", rk["bynavn"].ToString());
							addXML(ref b, "postnummer", rk["postnr"].ToString());
							addXML(ref b, "postdistrikt", rk["postby"].ToString());
							addXML(ref b, "easting", rk["stald_x_koor"].ToString());
							addXML(ref b, "northing", rk["stald_y_koor"].ToString());

							cr.AppendChild(b);
						}
					}
					kd.AppendChild(cr);
				}
			}
			catch (Exception e)
			{
				skrivFejl(e.Message);
			}
			return doc;
		}

		[WebMethod]
		public XmlDocument chr_adresse_soeg(string chr_nr="", string vejnavn = "", string husnr = "", string postnr = "")
		{
			startXML("Adressedata");
			util ut = new util();
			if (chr_nr != "")	// CHR-søgning 
			{
				DataBase db = ut.db_query("SELECT distinct a.chr_nr, a.vej, a.bynavn, a.postnr, a.postby, a.stald_x_koor, a.stald_y_koor, b.veterinaersektion, st_x(st_transform(a.geom, 4326)) as lon, st_y(st_transform(a.geom, 4326)) as lat FROM chr.chr_samlet a left join  dagi.veterinaersektioner_webgis_2012 b on st_within(a.geom, b.geom) WHERE a.chr_nr in(" + chr_nr + ") order by a.chr_nr"); 
				if (db.Result.Rows.Count == 0)
				{
					skrivFejl("CHR findes ikke");
					return doc;
				}
				addXML(ref kd, "antal", db.Result.Rows.Count.ToString());
				addXML(ref kd, "faktisk_antal", db.Result.Rows.Count.ToString());
				XmlElement adr = doc.CreateElement(string.Empty, "Adresser", string.Empty);
				foreach (DataRow rk in db.Result.Rows)
				{
					XmlElement ad = doc.CreateElement(string.Empty, "Adresse", string.Empty);
					// ??? var oprindelig opdelt i vejnavn og husnr, men ikke korrekt
					addXML(ref ad, "adresse", rk["vej"].ToString());
					addXML(ref ad, "postnummer", rk["postnr"].ToString());
					addXML(ref ad, "postdistrikt", rk["postby"].ToString());
					addXML(ref ad, "supplerende_stednavn", rk["bynavn"].ToString());
					addXML(ref ad, "easting", rk["stald_x_koor"].ToString());
					addXML(ref ad, "northing", rk["stald_y_koor"].ToString());
					addXML(ref ad, "chr_nr", rk["chr_nr"].ToString());
					addXML(ref ad, "Veterinærsektion", rk["veterinaersektion"].ToString());
					addXML(ref ad, "lat", rk["lat"].ToString());
					addXML(ref ad, "lon", rk["lon"].ToString());
					adr.AppendChild(ad);
				}
				kd.AppendChild(adr);
			}
			else // adressesøgning
			{
			vejnavn= vejnavn.Trim();
			vejnavn= vejnavn.Replace("%20", " ");

			husnr = husnr.ToUpper().Replace("%20", "");
			if (husnr.ToLower() == husnr)
				husnr +="*"; // søg på alle bogstaver

			// kontroller at alle inputparametre er korrekt udfyldt
			if (vejnavn=="" || husnr=="" || postnr=="")	
			{
				skrivFejl("Både vejnavn, husnr og postnr skal udfyldes");	
				return doc;
			}

			// kontroller at postnummeret er et firecifret tal
			int post_tal =0;
			int.TryParse(postnr,out post_tal);
			if (post_tal < 1000 || post_tal > 9999)
			{
				skrivFejl("Postnr skal udfyldes med et firecifret tal");	
				return doc;
			}
			double x = 0;
			double y = 0;
			// kald til Kortforsyningen
			HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create("http://kortforsyningen.kms.dk/?servicename=RestGeokeys_v2&method=adresse&f=xml&vejnavn=" + vejnavn + "&husnr=" + husnr + "&postnr=" + post_tal.ToString() + "&hits=50&geometry=true&login=" + ConfigurationManager.AppSettings["KMSlogin"] + "&password=" + ConfigurationManager.AppSettings["KMSpassword"]);
			Stream res = req.GetResponse().GetResponseStream();
			StreamReader reader = new StreamReader(res);
			string ksvar = reader.ReadToEnd();
			try
			{
				XmlDocument xml = new XmlDocument();
				xml.LoadXml(ksvar);
				 XmlNodeList featList = xml.GetElementsByTagName("Feature");
				// antal
				int antal = featList.Count;
				addXML(ref kd, "Antal", antal.ToString());
				int a = 0;
				if (antal == 0 )
				{
					skrivFejl(">Adressen er ikke gyldig");
					return doc;
				}
				XmlElement adr = doc.CreateElement(string.Empty, "Adresser", string.Empty);
				for (int i = 0; i < antal; i++)
				{
					XmlElement ad = doc.CreateElement(string.Empty, "Adresse", string.Empty);
					string vej =featList[i].SelectSingleNode("properties/vej/navn").InnerText;
					addXML(ref ad, "Vejnavn", vej);
					string hu=featList[i].SelectSingleNode("properties/husnr").InnerText;
					addXML(ref ad, "Husnr", hu);
					string po =featList[i].SelectSingleNode("properties/postdistrikt/kode").InnerText;
					addXML(ref ad, "Postnr", po);
					addXML(ref ad, "Postdistrikt", featList[i].SelectSingleNode("properties/postdistrikt/navn").InnerText);
					// kortforsyningen giver ikke supllerende stednavn
					// addXML(ref ad, "Supplerende_stednavn", featList[i].SelectSingleNode("properties/postdistrikt/navn").InnerText);
					addXML(ref ad, "Kommunenavn", featList[i].SelectSingleNode("properties/kommune/navn").InnerText);
					string[] geo = featList[i].SelectSingleNode("geometry").InnerText.Replace("POINT(", "").Replace(")", "").Split(' ');
					x = double.Parse(geo[0]);
					y = double.Parse(geo[1]);
					addXML(ref ad, "Northing", geo[1]);
					addXML(ref ad, "Easting", geo[0]);
					// find chr ud fra adresse
					// hvis der er et bogstav i husnr, skal der et mellemrum ind
					foreach(char c in hu)
					{
						if (Char.IsLetter(c)) 
						{
							hu = hu.Substring(0,hu.Length-1) + " " + hu.Substring(hu.Length-1,1);
							break;
						}
					}
					string chr = ut.db_enkeltopslag("select chr_nr from chr.chr_samlet where vej ='" + vej + " " + hu +"' and postnr=" + postnr + " limit 1");
					if (chr == "0") chr = "";
					addXML(ref ad, "chr_nummer", chr);
					// find veterinærsektion
					string vet = ut.db_enkeltopslag("select veterinaersektion from dagi.veterinaersektioner_webgis_2012 where st_within(st_GeomFromText('POINT(" + x + " " + y + ")',25832),geom) limit 1");
					if (vet == "0") vet = "";
					addXML(ref ad, "Veterinærsektion", vet);
					a++;
					adr.AppendChild(ad);
				}
				addXML(ref adr, "faktisk_antal", a.ToString());
				kd.AppendChild(adr);
			}
			catch (Exception e)
			{
				skrivFejl(e.Message);
			}
			}
			return doc;
		}
	
		private void addXML(ref XmlElement over, string titel, string text)
		{
			XmlElement f = doc.CreateElement(string.Empty, titel, string.Empty);
			XmlText t = doc.CreateTextNode(text);
			f.AppendChild(t);
			over.AppendChild(f);
		}
		
		private void skrivFejl(string txt)
		{
				// fejlmeddelelse
					XmlElement f = doc.CreateElement(string.Empty, "Fejl", string.Empty);
					XmlText t = doc.CreateTextNode(txt);
					f.AppendChild(t);
					kd.AppendChild(f);
		}


	}
}
