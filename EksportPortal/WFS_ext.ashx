﻿<%@ WebHandler Language="C#" Class="WFS" %>

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.SessionState;
using System.Drawing;
using System.Drawing.Imaging;

// NB: kalder ikke proxyservice, koden er flyttet herind

/// <summary>
/// A proxy class to proxy HTTP calls to a local WMS server installation, e.g. GeoServer
/// </summary>
public class WFS : IHttpHandler
{
    /// <summary>
    /// Implements the IHttpHandler.ProcessRequest
    /// WMSServerUrl configured in the web.config file is used as the destination host url
    /// </summary>
    /// <param name="context"></param>
    public void ProcessRequest(HttpContext context)
    {
        // her kommer vi kun til egne lag
        string WFSServerUrl = ConfigurationManager.AppSettings["WFSServerUrl"];
        string url = WFSServerUrl + context.Request.Url.Query;
        WebRequest request = WebRequest.Create(url);
        request.Credentials = CredentialCache.DefaultCredentials;
        request.Method = context.Request.RequestType;
        request.ContentType ="text/xml";
        System.Diagnostics.Debug.Print(request.Method  + " - URL: " + url);
        try
        {
            // hvis det er et POST kald, skal InputStream sendes videre
            if (context.Request.RequestType == "POST")
            {
                byte[] buffer = new byte[context.Request.InputStream.Length];
                context.Request.InputStream.Read(buffer, 0, buffer.Length);
                request.ContentLength = buffer.Length;
                Stream reqstr = request.GetRequestStream();
                reqstr.Write(buffer, 0, buffer.Length);
                reqstr.Close();
                //using (var reader = new StreamReader(context.Request.InputStream))
                //{
                //    // This will equal to "charset = UTF-8 & param1 = val1 & param2 = val2 & param3 = val3 & param4 = val4"
                //    string values = reader.ReadToEnd();
                //    System.Diagnostics.Debug.Print("PARAMETRE: " + values);
                //}
                System.Diagnostics.Debug.Print("IND: " + System.Text.Encoding.UTF8.GetString(buffer, 0, buffer.Length));
                //buffer = new byte[request.GetRequestStream().Length];
                //Stream reqstr = request.GetRequestStream();
                //reqstr.Write(buffer, 0, buffer.Length);
                //System.Diagnostics.Debug.Print("IND: " + System.Text.Encoding.UTF8.GetString(buffer, 0, buffer.Length));
            }


            // Get the response and content-type
            WebResponse response = request.GetResponse();
            Stream stream = response.GetResponseStream();

            // Set expires to the past if no expires has been given.
            if (response.Headers["Expires"] == null)
            {
                response.Headers["Expires"] = (new DateTime(1970, 1, 1)).ToString();
                context.Response.CacheControl = null;
            }

            DateTime expires = DateTime.Parse(response.Headers["Expires"].ToString());
            TimeSpan duration = new TimeSpan(0); ;
            if (expires > DateTime.Now)
            {
                duration = expires.Subtract(DateTime.Now);
            }

            context.Response.Cache.SetExpires(expires);
            context.Response.Cache.SetMaxAge(duration);

            // "TEXT" proxy
            // Always read and write stream as UTF-8, i.e. the external WFS/WMS services must use UTF-8 encoding
            context.Response.ContentType = response.ContentType.Replace("iso-8859-1", "utf-8");
            context.Response.Charset = "UTF-8";// ((HttpWebResponse)response).CharacterSet;
            context.Response.ContentEncoding = Encoding.GetEncoding(context.Response.Charset);

            // Choose encoding based on correspondance in the URL. Very naive implementation!!!
            Encoding currentEncoding;
            //if (url.IndexOf("kort.plansystem.dk") >= 0)
            if (url.IndexOf("wfs.plansystem.dk") >= 0)
            {
                currentEncoding = Encoding.GetEncoding("ISO-8859-1");
            }
            else
            {
                // Use UTF-8 as default encoding.
                currentEncoding = Encoding.UTF8;
            }

            // Open the stream using a StreamReader.
            StreamReader reader;
            // Depending on the encoding, the correct scheme is chosen.
            if (currentEncoding == Encoding.GetEncoding("ISO-8859-1"))
                reader = new StreamReader(stream, Encoding.GetEncoding("ISO-8859-1"));
            else // UTF-8
                reader = new StreamReader(stream, context.Response.ContentEncoding); // Default chosen by viskort.dk

            // Read the stream and close the reader.
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            responseFromServer = responseFromServer.Replace(ConfigurationManager.AppSettings["WFSfra"], ConfigurationManager.AppSettings["WFStil"]);
            // Convert encoding if necessary.
            if (currentEncoding == Encoding.GetEncoding("ISO-8859-1"))
            {
                Encoding targetEncoding = Encoding.UTF8;
                byte[] currentEncodingBytes = currentEncoding.GetBytes(responseFromServer);
                byte[] targetEncodingBytes = Encoding.Convert(currentEncoding, targetEncoding, currentEncodingBytes);
                responseFromServer = targetEncoding.GetString(targetEncodingBytes);
            }
            System.Diagnostics.Debug.Print("SVAR: " + responseFromServer + "\r\n");

            // Write the response to the original request
            context.Response.Write(responseFromServer);

            context.Response.Flush();

            stream.Close();
            response.Close();
        }
        catch (WebException ex)
        {
            // In case of a web exception the server error (code=500) is returned.
            context.Response.Write(ex.Message);
            context.Response.StatusCode = 500;
            return;
        }


    }

    /// <summary>
    /// Implements IHttpHandler.IsReusable
    /// Returns false
    /// </summary>
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}



//public class WFS : IHttpHandler, IRequiresSessionState
//{
//    /// <summary>
//    /// Implements the IHttpHandler.ProcessRequest
//    /// WMSServerUrl configured in the web.config file is used as the destination host url
//    /// </summary>
//    /// <param name="context"></param>
//    public void ProcessRequest(HttpContext context)
//    {
//        // her kommer vi kun til egne lag
//        string WFSServerUrl = ConfigurationManager.AppSettings["WFSServerUrl"];
//        string url = WFSServerUrl + context.Request.Url.Query;
//        WebRequest request = WebRequest.Create(url);
//        request.Credentials = CredentialCache.DefaultCredentials;
//        request.Method = context.Request.RequestType;
//        request.ContentType ="text/xml";
//        System.Diagnostics.Debug.Print(request.Method  + " - URL: " + url);
//        try
//        {
//            // hvis det er et POST kald, skal InputStream sendes videre
//            if (context.Request.RequestType == "POST")
//            {
//                byte[] buffer = new byte[context.Request.InputStream.Length];
//                context.Request.InputStream.Read(buffer, 0, buffer.Length);
//                request.ContentLength = buffer.Length;
//                Stream reqstr = request.GetRequestStream();
//                reqstr.Write(buffer, 0, buffer.Length);
//                reqstr.Close();
//                //using (var reader = new StreamReader(context.Request.InputStream))
//                //{
//                //    // This will equal to "charset = UTF-8 & param1 = val1 & param2 = val2 & param3 = val3 & param4 = val4"
//                //    string values = reader.ReadToEnd();
//                //    System.Diagnostics.Debug.Print("PARAMETRE: " + values);
//                //}
//                System.Diagnostics.Debug.Print("IND: " + System.Text.Encoding.UTF8.GetString(buffer, 0, buffer.Length));
//                //buffer = new byte[request.GetRequestStream().Length];
//                //Stream reqstr = request.GetRequestStream();
//                //reqstr.Write(buffer, 0, buffer.Length);
//                //System.Diagnostics.Debug.Print("IND: " + System.Text.Encoding.UTF8.GetString(buffer, 0, buffer.Length));
//            }


//            // Get the response and content-type
//            WebResponse response = request.GetResponse();
//            Stream stream = response.GetResponseStream();

//            // Set expires to the past if no expires has been given.
//            if (response.Headers["Expires"] == null)
//            {
//                response.Headers["Expires"] = (new DateTime(1970, 1, 1)).ToString();
//                context.Response.CacheControl = null;
//            }

//            DateTime expires = DateTime.Parse(response.Headers["Expires"].ToString());
//            TimeSpan duration = new TimeSpan(0); ;
//            if (expires > DateTime.Now)
//            {
//                duration = expires.Subtract(DateTime.Now);
//            }

//            context.Response.Cache.SetExpires(expires);
//            context.Response.Cache.SetMaxAge(duration);

//            if (response.ContentType.ToLower().StartsWith("image") || response.ContentType == "")
//            {
//                // "IMAGE" proxy               
//                context.Response.ContentType = response.ContentType;

//                Image image = Image.FromStream(stream);
//                ImageFormat format = image.RawFormat;

//                MemoryStream ms = new MemoryStream();
//                image.Save(ms, format);
//                ms.WriteTo(context.Response.OutputStream);
//            }
//            else
//            {
//                // "TEXT" proxy
//                // Always read and write stream as UTF-8, i.e. the external WFS/WMS services must use UTF-8 encoding
//                context.Response.ContentType = response.ContentType.Replace("iso-8859-1", "utf-8");
//                context.Response.Charset = "UTF-8";// ((HttpWebResponse)response).CharacterSet;
//                context.Response.ContentEncoding = Encoding.GetEncoding(context.Response.Charset);

//                // Choose encoding based on correspondance in the URL. Very naive implementation!!!
//                Encoding currentEncoding;
//                //if (url.IndexOf("kort.plansystem.dk") >= 0)
//                if (url.IndexOf("wfs.plansystem.dk") >= 0)
//                {
//                    currentEncoding = Encoding.GetEncoding("ISO-8859-1");
//                }
//                else
//                {
//                    // Use UTF-8 as default encoding.
//                    currentEncoding = Encoding.UTF8;
//                }

//                // Open the stream using a StreamReader.
//                StreamReader reader;
//                // Depending on the encoding, the correct scheme is chosen.
//                if (currentEncoding == Encoding.GetEncoding("ISO-8859-1"))
//                    reader = new StreamReader(stream, Encoding.GetEncoding("ISO-8859-1"));
//                else // UTF-8
//                    reader = new StreamReader(stream, context.Response.ContentEncoding); // Default chosen by viskort.dk

//                // Read the stream and close the reader.
//                string responseFromServer = reader.ReadToEnd();
//                reader.Close();
//                responseFromServer = responseFromServer.Replace(ConfigurationManager.AppSettings["WFSfra"], ConfigurationManager.AppSettings["WFStil"]);
//                // Convert encoding if necessary.
//                if (currentEncoding == Encoding.GetEncoding("ISO-8859-1"))
//                {
//                    Encoding targetEncoding = Encoding.UTF8;
//                    byte[] currentEncodingBytes = currentEncoding.GetBytes(responseFromServer);
//                    byte[] targetEncodingBytes = Encoding.Convert(currentEncoding, targetEncoding, currentEncodingBytes);
//                    responseFromServer = targetEncoding.GetString(targetEncodingBytes);
//                }
//                System.Diagnostics.Debug.Print("SVAR: " + responseFromServer + "\r\n");

//                // Write the response to the original request
//                context.Response.Write(responseFromServer);

//                context.Response.Flush();
//            }

//            stream.Close();
//            response.Close();
//        }
//        catch (WebException ex)
//        {
//            // In case of a web exception the server error (code=500) is returned.
//            context.Response.Write(ex.Message);
//            context.Response.StatusCode = 500;
//            return;
//        }


//    }

//    /// <summary>
//    /// Implements IHttpHandler.IsReusable
//    /// Returns false
//    /// </summary>
//    public bool IsReusable
//    {
//        get
//        {
//            return false;
//        }
//    }

//}