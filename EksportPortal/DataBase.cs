﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Runtime.CompilerServices;

using Npgsql;

namespace EksportPortal
{
    /// <summary>
    /// Summary description for DataBase
    /// </summary>
    public class DataBase
    {
		private long _id;
		private string _sqlstatement;
        private string _connectionString;
        private NpgsqlConnection _dbConnection;
        private NpgsqlTransaction _dbTransaction;
        private List<string> _keys;
        private Dictionary<string, object> _values;
        private Dictionary<string, NpgsqlTypes.NpgsqlDbType> _datatypes;
        public DataTable Result { get; set; }

        /// <summary>
        /// Constructor, which should not be used directly.
        /// </summary>
        public DataBase()
        {
            Reset();
        }

		//private void Log(string text,
		//				[CallerFilePath] string file = "",
		//				[CallerMemberName] string member = "",
		//				[CallerLineNumber] int line = 0)
		//{
		//	System.Diagnostics.Debug.Print("{0}_{1}({2}): {3}", System.IO.Path.GetFileName(file), member, line, text);
		//}

        /// <summary>
        /// Constructor which takes a SQL statement and a connectionstring.
        /// </summary>
        /// <param name="sql">Parameter which contains the SQL statement which should be fired. Use ':key' to notify of an incoming parameter - to avoid SQL injection.</param>
        /// <param name="connectionstring">A customised connection string.</param>
        public DataBase(string sql, string connectionstring, [CallerFilePath] string file = "", [CallerMemberName] string member = "", [CallerLineNumber] int line = 0) : this()
        {
            _connectionString = connectionstring;

            // Open the connection to the database.
            _dbConnection = new NpgsqlConnection(_connectionString);
            _dbConnection.Open();

			// skriv åbningskald i db
			if (System.Configuration.ConfigurationManager.AppSettings["LogDB"] == "1")
			{
				try
				{
					_id = SingleSelect<long>("insert into db_call_log(file, procedure, line, sql) values('" + file + "', '" + member + "', " + line + ", '" + sql.Replace("'", "''") + "') returning id");
					Reset();
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.Print(e.Message);
				}
			}
			_sqlstatement = sql;
		}

        /// <summary>
        /// Constructor which takes a connectionstring.
        /// </summary>        
        /// <param name="connectionstring">A customised connection string.</param>
		public DataBase(string connectionstring, [CallerLineNumber] int line = 0, [CallerFilePath] string file = "", [CallerMemberName] string member = "")
			: this()
        {            
            _connectionString = connectionstring;

            // Open the connection to the database.
            _dbConnection = new NpgsqlConnection(_connectionString);
            _dbConnection.Open();
			// skriv åbningskald i db
			if (System.Configuration.ConfigurationManager.AppSettings["LogDB"] == "1")
			{
				try
				{
					_id = SingleSelect<long>("insert into db_call_log(file, procedure, line) values('" + file + "', '" + member + "', " + line + ") returning id");
					Reset();
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.Print(e.Message);
				}
			}
		}

        /// <summary>
        /// Begin a new transaction.
        /// </summary>
        public void BeginTransaction()
        {
            _dbTransaction = _dbConnection.BeginTransaction();
        }

        /// <summary>
        /// Commit the current transaction.
        /// </summary>
		public void CommitTransaction()
        {
            if (_dbTransaction != null)
            {
                _dbTransaction.Commit();
            }
        }

        /// <summary>
        /// Rollback the current transaction.
        /// </summary>
		public void RollbackTransaction()
        {
            if (_dbTransaction != null)
            {
                _dbTransaction.Rollback();
            }
        }

        /// <summary>
        /// Insert a new SQL statement, which is to be executed/queried. Any old statements and results will be lost.
        /// </summary>
        /// <param name="sql">The SQL statemenet.</param>
        public void SetSQLStatement(string sql)
        {
            Reset();
            _sqlstatement = sql;
        }

		public string GetSQLStatement()
		{
			return _sqlstatement;
		}

        /// <summary>
        /// Reset the current parameters used by the connection, allowing a new SQL statement to be made.
        /// </summary>
        private void Reset()
        {
            _keys = new List<string>();
            _values = new Dictionary<string, object>();
            _datatypes = new Dictionary<string, NpgsqlTypes.NpgsqlDbType>();

            // The last result is destroyed.
            Result = null;            
        }

        /// <summary>
        /// Method for adding a parameter to avoid SQL injections from this value.
        /// </summary>
        /// <param name="key">The identifier used in the SQL statement</param>
        /// <param name="value">The value which should be inserted.</param>
        /// <param name="dataType">The data type of the value.</param>
        public void Add(string key, object value, NpgsqlTypes.NpgsqlDbType dataType)
        {
            _keys.Add(key);
            _values.Add(key, value);
            _datatypes.Add(key, dataType);
        }

		/// <summary>
        /// Generate a new command object. This is used to avoid SQL injections.
        /// </summary>        
        /// <returns>A command object</returns>
        private NpgsqlCommand prepareCommand()
        {
            NpgsqlCommand dbCommand = new NpgsqlCommand(_sqlstatement, _dbConnection);            

            return prepareCommand(dbCommand);
        }

        /// <summary>
        /// Modify a command object. This is used to avoid SQL injections.
        /// </summary>        
        /// <param name="dbCommand">The source command object.</param>
        /// <returns>A command object</returns>        
        private NpgsqlCommand prepareCommand(NpgsqlCommand dbCommand)
        {
            // Add parameters.
            foreach (string _key in _keys)
            {
                dbCommand.Parameters.Add(_key, _datatypes[_key]);
            }

            // Prepare the command.
            dbCommand.Prepare();

            // Add parameter values.
            foreach (string _key in _keys)
            {
                dbCommand.Parameters[_key].Value = _values[_key];
            }

            return dbCommand;
        }

        /// <summary>
        /// Closes the database connection. It is vital that this method is called after accessing the database!
        /// </summary>
        public void Close()
        {
			if (System.Configuration.ConfigurationManager.AppSettings["LogDB"] == "1")
			{
				DataTable Res = Result;
				Reset();
				_sqlstatement = "update db_call_log set closed=true where id=" + _id;
				PerformExecution();
				Result = Res;
			}
			_dbConnection.Close();            
        }

        /// <summary>
        /// Method which performs the database operations. Use this one for INSERT, UPDATE and DELETE. Use Query() for SELECT.
        /// </summary>
        /// <returns>Returns if the operation was a success.</returns>
        public int Execute()
        {
            return PerformExecution();
        }

        /// <summary>
        /// Method which performs the database operations. Use this one for INSERT, UPDATE and DELETE. Use Query() for SELECT.
        /// </summary>
        /// <param name="closeConnection">Set to true to close the connection after the query has been performed.</param>
        /// <returns>Returns the number of rows affected.</returns>
        public int Execute(bool closeConnection)
        {
            int numberOfAffectedRows = PerformExecution();

            // Close the connection if specified.
            if (closeConnection)
            {
                Close();
            }

            return numberOfAffectedRows;
        }

        /// <summary>
        /// Perform an execution.
        /// </summary>
        /// <returns>Returns the number of rows affected.</returns>
        private int PerformExecution()
        {
            NpgsqlCommand dbCommand = prepareCommand();

            return dbCommand.ExecuteNonQuery();            
        }

        /// <summary>
        /// Method which performs the database operations. Use this one for SELECT. Use Execute() for INSERT, UPDATE and DELETE.
        /// To get the result, use Result().
        /// </summary>        
        /// <returns>Returns if the operation was a success.</returns>        
        public bool Query()
        {
            return PerformQuery();
        }

        /// <summary>
        /// Method which performs the database operations. Use this one for SELECT. Use Execute() for INSERT, UPDATE and DELETE.
        /// To get the result, use Result().
        /// </summary>        
        /// <param name="closeConnection">Set to true to close the connection after the query has been performed.</param>
        /// <returns>Returns if the operation was a success.</returns>        
        public bool Query(bool closeConnection)
        {
            bool success = PerformQuery();

            // Close the connection if asked.
            if (closeConnection)
            {
                Close();
            }

            return success;
        }

        /// <summary>
        /// Perform a SQL query.
        /// </summary>
        /// <returns>True if the query was a success.</returns>
        private bool PerformQuery()
        {
            try
            {
                NpgsqlDataAdapter da = new NpgsqlDataAdapter(_sqlstatement, _dbConnection);
                da.SelectCommand = prepareCommand(da.SelectCommand);

                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables.Count > 0)
                {
                    Result = ds.Tables[0];
                }                
            }
            catch
            {
                return false;
            }
            return true;
            
            //NpgsqlCommand dbCommand = prepareCommand();

            //List<Dictionary<string, object>> listOfRows = new List<Dictionary<string, object>>();
            
            //try
            //{
            //    NpgsqlDataReader dbReader = dbCommand.ExecuteReader();
            //    while (dbReader.Read())
            //    {
            //        Dictionary<string, object> row = new Dictionary<string, object>();
            //        for (int i = 0; i < dbReader.FieldCount; i++)
            //        {
            //            if (dbReader.IsDBNull(i))
            //            {
            //                row.Add(dbReader.GetName(i), null);
            //            }
            //            else
            //            {
            //                row.Add(dbReader.GetName(i), dbReader[i]);
            //            }
            //        }

            //        listOfRows.Add(row);
            //    }

            //}
            //catch
            //{
            //    return false;
            //}

            //Result = new QueryResult(listOfRows);
            //return true;
        }

        /// <summary>
        /// Method which does a select return only ONE value!
        /// </summary>
        /// <typeparam name="T">The type of the expected value.</typeparam>
        /// <param name="sql">The SQL select statement to be performed.</param>
        /// <returns>The value returned by the selection.</returns>
        public T SingleSelect<T>(string sql)
        {
			Reset();
			_sqlstatement = sql;
            NpgsqlCommand dbCommand = prepareCommand();

            try
            {
                return (T)dbCommand.ExecuteScalar();
            }
            catch
            {
                return default(T);
            }
            finally
            {
                this.Reset();
            }                        
        }

        /// <summary>
        /// Method to handle a generic parameter value and insert NULL if the value is empty or null.
        /// </summary>
        /// <typeparam name="T">The type of the parameter</typeparam>
        /// <param name="item">The value of the parameter as a string</param>
        /// <returns></returns>
        public object GetValue<T>(string item)
        {
            if (item == null || item.Length == 0)
            {
                return DBNull.Value;
            }

            return (T)Convert.ChangeType(item, typeof(T));
        }

        /// <summary>
        /// Method to handle the WKT geometry while inserting as a parameter.
        /// </summary>
        /// <param name="item">The WKT formatted geometry</param>
        /// <returns></returns>
        public string GetGeomValue(string item)
        {
            if (item == null || item.Length == 0)
            {
                return "null";
            }

            return "'" + item + "'";
        }

		//public void bit2bool()
		//{
		//    // benyttes ikke her, kun i MS
		//}
			
			
			
	/// <summary>
		/// Method to get next value of a sequence - returned as int.
		/// </summary>
		/// <param name="sequenceName">The name of the sequence</param>
		/// <returns></returns>


		#region sequences
		public int GetNextval(string sequenceName)
		{
			Reset();
			return (int)SingleSelect<Int64>("SELECT nextval('" + sequenceName + "') AS next_val");
		}

		/// <summary>
		/// Method to get next value of a sequence - returned as Int64.
		/// </summary>
		/// <param name="sequenceName">The name of the sequence</param>
		/// <returns></returns>
		public Int64 GetNextval64(string sequenceName)
		{
			Reset();
			return SingleSelect<Int64>("SELECT nextval('" + sequenceName + "') AS next_val");
		}
		#endregion

		# region users_roles
		public bool CreateUser(string UserName, string Password, string DBname)
		{
			try
			{
				Reset();
				_sqlstatement = "SELECT usename from pg_user where usename ='" + UserName + "'";
				PerformQuery();
				if (Result ==null || Result.Rows.Count == 0)
				{
					_sqlstatement = "CREATE ROLE " + UserName + " WITH PASSWORD '" + Password + "' INHERIT LOGIN";
					PerformExecution();
				}
				return true;
			}
			catch //(Exception e)
			{
				return false;
			}
		}
		// her listes alle brugere på serveren, ikke kun dem i den aktuelle database
		// navnet kan få domæne-teksten med i like
		public bool ListUsers(string UserName)
		{
			Reset();
			// either all users or a specific user returned as table
			_sqlstatement = "SELECT usename from pg_user where usename ";
			if (UserName == "")
				_sqlstatement += " like 'u_%'";
			else
				_sqlstatement += " ='" + UserName + "'";
			try
			{
				PerformQuery();
				return Result.Rows.Count > 0;
			}
			catch //(Exception e)
			{
				return false;
			}
		}

			public bool CreateRole(string RoleName)
			{
				Reset();
				_sqlstatement = "SELECT rolname from pg_roles where rolname ='" + RoleName + "'";
				try
				{
					PerformQuery();
					if (Result == null || Result.Rows.Count == 0)
					{
						_sqlstatement = "CREATE ROLE " + RoleName;
						PerformExecution();
					}
					return true;
				}
				catch //(Exception e)
				{
					return false;
				}
			}

			public bool DeleteRole(string RoleName)
			{
				// alle medlemmer af rollen slettes automatisk

				try
				{
					Reset();
					// alle rollens rettigheder skal fjernes fra den
					_sqlstatement = "drop owned by '" + RoleName + "'";
					PerformExecution();
					//slet selve rollen					
					_sqlstatement = "DROP ROLE IF EXISTS '" + RoleName + "'";
					PerformExecution();
					return true;
				}
				catch //(Exception e)
				{
					return false;
				}
			}

			//public bool ListRoleMembers(string RoleName)
			//{
			//  // ikke implementeret
			//  _sqlstatement = "SELECT a.name FROM sys.database_principals a, sys.database_role_members b " +
			//                " where a.principal_id=b.member_principal_id and USER_NAME(b.role_principal_id) ='" + RoleName + "'";
			//  try
			//  {
			//    return PerformQuery();
			//  }
			//  catch //(Exception e)
			//  {
			//    return false;
			//  }
			//}

			public bool AddUserToRole(string RoleName, string UserName)
			{
				Reset();
				_sqlstatement = "GRANT " + RoleName + " TO " + UserName;
				try
				{
					PerformExecution();
					return true;
				}
				catch //(Exception e)
				{
					return false;
				}
			}

			public bool RemoveUserFromRole(string RoleName, string UserName)
			{
				Reset();
				_sqlstatement = "REVOKE " + RoleName + " from " + UserName;
				try
				{
					PerformExecution();
					return true;
				}
				catch //(Exception e)
				{
					return false;
				}
			}
			#endregion

			#region tables
			public bool ListTables(string SchemaName, string TableName)
			{
				Reset();
				// either all tables in the schema or a specific table returned as table
				_sqlstatement = "SELECT * FROM information_schema.tables WHERE table_schema = '" + SchemaName + "'";
				if (TableName != "")
					_sqlstatement += " and table_name = '" + TableName + "'";
				try
				{
					return PerformQuery();
				}
				catch //(Exception e)
				{
					return false;
				}
			}

			public void DropTable(string SchemaName, string TableName)
			{
                // cascade burde tage alle underordnede objekter med automatisk, dvs constraints og triggers og views
				try
				{
					Reset();
					_sqlstatement = "drop table if exists " + SchemaName + "." + TableName + " cascade";
					PerformExecution();
				}
				catch //(Exception e)
				{
				}
			}
			#endregion


		}

}
	
	
