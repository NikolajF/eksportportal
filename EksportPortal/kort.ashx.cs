﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data;
//using LE34.Data.Access;

namespace EksportPortal
{
	/// <summary>
	/// Summary description for kort
	/// </summary>
	public class kort : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
			context.Response.ContentType = "image/png";
			System.Drawing.Image img = null;
			Graphics g = null;
			int bredde = 500;
			int højde = 500;
			try
			{
				double xmin = double.Parse(context.Request.QueryString["xmin"]);
				double ymin = double.Parse(context.Request.QueryString["ymin"]);
				double xmax = double.Parse(context.Request.QueryString["xmax"]);
				double ymax = double.Parse(context.Request.QueryString["ymax"]);
				string kort = context.Request.QueryString["kort"];
				switch (kort)
				{
					case "oversigt":
						bredde = 180;
						højde = 150;
						double kxmin = double.Parse(ConfigurationManager.AppSettings["oversigtskort_xmin"]);
						double kymin = double.Parse(ConfigurationManager.AppSettings["oversigtskort_ymin"]);
						double kxmax = double.Parse(ConfigurationManager.AppSettings["oversigtskort_xmax"]);
						double kymax = double.Parse(ConfigurationManager.AppSettings["oversigtskort_ymax"]);
						double kfaktor = bredde / (kxmax - kxmin);
						int x1 = (int)Math.Round((xmin - kxmin) * kfaktor, 0);
						int y1 = højde - (int)Math.Round((ymin - kymin) * kfaktor, 0);
						int x2 = (int)Math.Round((xmax - kxmin) * kfaktor, 0);
						int y2 = højde - (int)Math.Round((ymax - kymin) * kfaktor, 0);
//						img = Image.FromFile(ConfigurationManager.AppSettings["oversigtskort"]);
						img = Image.FromFile(HttpRuntime.AppDomainAppPath + "\\bin\\dk_land_gra.PNG");
						g = Graphics.FromImage(img);
						g.SmoothingMode = SmoothingMode.AntiAlias;
						g.InterpolationMode = InterpolationMode.HighQualityBicubic;
						g.PixelOffsetMode = PixelOffsetMode.HighQuality;
						if (x2 - x1 > 10000)
						{
							Pen p = new Pen(Color.Red, 3);
							p.DashStyle = DashStyle.Dot;
							g.DrawRectangle(p, x1, y2, x2 - x1, y1 - y2);
						}
						else
						{
							g.DrawString("X", new Font("Verdana", 15), Brushes.Red, new RectangleF(x1 - 9, y2 - 19, 20, 21));
						}
						g.Flush();
						break;
					case "detail":
					case "luft":
						string chr_nr = context.Request.QueryString["chr_nr"];
						util ut = new util();
						DataBase db = ut.db_query("select distinct chr_nr, stald_x_koor as x, stald_y_koor as y from chr.chr_samlet where chr_nr in(" + chr_nr + ")");
						// hent baggrundskort
						string mapURLrå ="";
						if (kort == "luft")
						{
							mapURLrå = ConfigurationManager.AppSettings["KMSServerUrl"] + "?LAYERS=orto_foraar&SERVICENAME=orto_foraar&VERSION=1.1.1" +
								 "&REQUEST=GetMap&SRS=EPSG%3A25832&FORMAT=image%2Fjpeg&SERVICE=WMS&login=" + ConfigurationManager.AppSettings["KMSlogin"] + "&password=" + ConfigurationManager.AppSettings["KMSpassword"] +
								 "&TILED=false&STYLES=&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&BBOX=" + xmin + "," + ymin + "," + xmax + "," + ymax +
								 "&WIDTH=" + bredde + "&HEIGHT=" + højde;
						}
						else
						{
							mapURLrå = ConfigurationManager.AppSettings["KMSServerUrl"] + "?LAYERS=dtk_skaermkort&SERVICENAME=topo_skaermkort&VERSION=1.1.1" +
								 "&REQUEST=GetMap&SRS=EPSG%3A25832&FORMAT=image%2Fjpeg&SERVICE=WMS&login=" + ConfigurationManager.AppSettings["KMSlogin"] + "&password=" + ConfigurationManager.AppSettings["KMSpassword"] +
								 "&TILED=false&STYLES=&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&BBOX=" + xmin + "," + ymin + "," + xmax + "," + ymax +
								 "&WIDTH=" + bredde + "&HEIGHT=" + højde;
						}
						ut.hentBillede(mapURLrå, ref img);
						g = Graphics.FromImage(img);

						// husnumre - hentes via gis34, samme lag som benyttes i poseidon intragis
						// http://www.gis34.dk/wms4.ashx?LAYERS=le34%3Av_husnumre_utm&FORMAT=image%2Fpng&SRS=EPSG%3A25832&REQUEST=GetMap&VERSION=1.1.1&SERVICE=wms&TRANSPARENT=TRUE&STYLES=&BBOX=712386.59353263,6183632.2968464,714218.23395645,6184548.1170584&WIDTH=2560&HEIGHT=1280
						g.CompositingMode = CompositingMode.SourceOver;
						string husnrURL = "http://www.gis34.dk/wms4.ashx?LAYERS=le34:v_husnumre_utm&FORMAT=image/png&SRS=EPSG:25832&REQUEST=GetMap&VERSION=1.1.1&SERVICE=wms&TRANSPARENT=TRUE&STYLES=&BBOX=" + xmin + "," + ymin + "," + xmax + "," + ymax +
								 "&WIDTH=" + bredde + "&HEIGHT=" + højde;
						Image hu = null;
						ut.hentBillede(husnrURL, ref hu);
						//hu.MakeTransparent();
						g.DrawImage(hu, new Point(0, 0));
						g.SmoothingMode = SmoothingMode.AntiAlias;
						g.InterpolationMode = InterpolationMode.HighQualityBicubic;
						g.PixelOffsetMode = PixelOffsetMode.HighQuality;

						//						g.Flush();
						// tegn punkterne
						double faktor = bredde / (xmax - xmin);

						foreach (DataRow rk in db.Result.Rows)
						{
							int x = (int)Math.Round((rk.Field<double>("x") - xmin) * faktor, 0);
							int y = 500 - (int)Math.Round((rk.Field<double>("y") - ymin) * faktor, 0);
							g.FillEllipse(Brushes.Red, x - 7, y - 7, 15, 15);
							g.DrawString(rk.Field<int>("chr_nr").ToString(), new Font("Verdana", 12), Brushes.Red, new RectangleF(x + 13, y - 8, 80, 18));

						}
						g.Flush();

						break;
				}
			}
			catch (Exception ex)
			{
				img = new Bitmap(bredde, højde);
				g = Graphics.FromImage(img);
				g.DrawString(ex.Message, new Font("Tahoma", 24), Brushes.Black, new RectangleF(10, 10, bredde - 20, højde - 20));
				g.Flush();

			}
			using (MemoryStream mem = new MemoryStream())
			{
				img.Save(mem, ImageFormat.Png);
				mem.Seek(0, SeekOrigin.Begin);

				context.Response.ContentType = "image/png";

				mem.CopyTo(context.Response.OutputStream, 4096);
				context.Response.Flush();
			}
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}