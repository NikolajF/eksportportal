﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Net;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
//using LE34.Data.Access;

namespace EksportPortal
{
	public class util
	{
		public util()
		{
			System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
		}
		
		public void hentBillede(string url, ref Image img)
		{
			WebClient wc = new WebClient();
			byte[] bytes = wc.DownloadData(url);
			MemoryStream ms = new MemoryStream(bytes);
			img = Image.FromStream(ms);
		}

		//public string db_exec(string sql, string tabel, string id)
		//{
		//	string svar = "";
		//	DataBase db = new DataBase(sql, ConfigurationManager.ConnectionStrings["bier_npgsql"].ConnectionString);
		//	//db.StringNotNull = true;
		//	//svar = db.Execute(tabel == "").ToString();
		//	//if (tabel != "")
		//	//{
		//	//  db.SetSQLStatement("insert into log_sql(log_raa, tabel, post, poster, tekst) values('" + logid + "', '" + tabel + "'," + id + ", " + svar + ", '" + anf(sql) + "')");
		//	//  db.Execute(true);
		//	//}
		//	try
		//	{
		//		svar = db.Execute(false).ToString();
		//		if (tabel != "")
		//		{
		//			db.SetSQLStatement("insert into log_sql(log_raa, tabel, post, poster, tekst) values('" + logid + "', '" + tabel + "'," + id + ", " + svar + ", '" + anf(sql) + "')");
		//			db.Execute(true);
		//		}
		//	}
		//	catch (Exception ex)
		//	{
		//		// skriv fejlen
		//		db.SetSQLStatement("insert into log_sql(log_raa, tabel, post, poster, tekst) values('" + logid + "', '" + tabel + "'," + id + ", -1, '" + anf(sql + "\r\n" + ex.Message) + "')");
		//		db.Execute(true);
		//	}
		//	return svar;
		//}

		public DataBase db_query(string sql)
		{
			DataBase db = new DataBase(sql, ConfigurationManager.ConnectionStrings["pg_local"].ConnectionString);
			//db.StringNotNull = true;
			db.Query(true);
			return db;
		}
		public DataRow db_enkeltrække(string sql)
		{
			DataRow svar = null;
			DataBase db = new DataBase(sql, ConfigurationManager.ConnectionStrings["pg_local"].ConnectionString);
			//db.StringNotNull = true;
			db.Query(true);
			if (db.Result != null && db.Result.Rows.Count > 0)
				svar = db.Result.Rows[0];
			return svar;
		}


		public string db_enkeltopslag(string sql)
		{

			string svar = "0";
			DataBase db = new DataBase(sql, ConfigurationManager.ConnectionStrings["pg_local"].ConnectionString);
			try
			{
				db.Query(true);
				// ??? hvis der ikke findes poster, fejler næste linje
				if (db.Result != null && db.Result.Rows.Count > 0)
				{
					svar = db.Result.Rows[0][0].ToString();
				}
			}
			catch
			{
			}

			return svar;
		}

		internal string backslash(string s)
		{
			return s.Replace("\\", "' || E'\\\\' || '");
		}
		internal string anf(string s)
		{
			s = s.Replace("'", "''");
			s = backslash(s);
			return s;
		}
	}
}