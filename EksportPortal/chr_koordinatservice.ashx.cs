﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Threading;

namespace EksportPortal
{
	/// <summary>
	/// Summary description for EksportPortal1
	/// </summary>
	public class EksportPortal1 : IHttpHandler
	{
		Color[] frv = new Color[] { Color.Black, Color.Yellow, Color.Green, Color.Blue, Color.Red, Color.LightGray, Color.Violet, Color.Cyan, Color.Brown, Color.White, Color.Orange };
		private class koordinater
		{
			public double x { get; set; }
			public double y { get; set; }
			public string bogstav { get; set; }
			public int farve { get; set; }
			public string label { get; set; }
		}
		int width = 0;
		int height = 0;

		public void ProcessRequest(HttpContext context)
		{
			Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
			context.Response.ContentType = "image/png";
			System.Drawing.Image img = null;
			try
			{
				width = int.Parse(context.Request.QueryString["width"]);
				height = int.Parse(context.Request.QueryString["height"]);
				int minwidth = int.Parse(context.Request.QueryString["minwidth"]);
				string points = context.Request.QueryString["points"];

				if (width < 100 || width > 9999) throw new Exception("Width ikke gyldig");
				if (height < 100 || height > 9999) throw new Exception("Height ikke gyldig");
				if (minwidth < 100 || minwidth > 1999) throw new Exception("Minwidth ikke gyldig");
				if (points == "") throw new Exception("Points ikke angivet");


				double xmin = 0;
				double ymin = 0;
				double xmax = 0;
				double ymax = 0;


				// split points
				string[] li = points.Split(';');
				// her er adskillelsen blot mellemrum - x - y - lagbogstavet - label inkl rækkefølgenummeret
				koordinater[] pkt = new koordinater[li.Length];
				for (int i = 0; i < li.Length; i++)
				{
					if (li[i].Trim() == "") throw new Exception("Punkt nr " + (i + 1) + " ikke udfyldt");

					string[] v = li[i].Split(' ');
					// x
					pkt[i] = new koordinater();
					pkt[i].x = double.Parse(v[0]);
					if (pkt[i].x < 100000 || pkt[i].x > 1000000) throw new Exception("Ugyldig x-koordinat i punkt nr " + (i + 1) + " (" + pkt[i].x + ")");
					if (pkt[i].x < xmin || xmin == 0) xmin = pkt[i].x;
					if (pkt[i].x > xmax || xmax == 0) xmax = pkt[i].x;
					// y
					pkt[i].y = double.Parse(v[1]);
					if (pkt[i].y < 6000000 || pkt[i].y > 7000000) throw new Exception("Ugyldig y-koordinat i punkt nr " + (i + 1) + " (" + pkt[i].y + ")");
					if (pkt[i].y < ymin || ymin == 0) ymin = pkt[i].y;
					if (pkt[i].y > ymax || ymax == 0) ymax = pkt[i].y;

					// bogstav
					pkt[i].bogstav = v[2].ToUpper();
					pkt[i].farve = "UABCDEFGHJK".IndexOf(v[2]);
					if (pkt[i].farve < 0) throw new Exception("Ugyldigt lagnavn i punkt nr " + (i + 1) + " (" + v[2] + ")");


					// label
					pkt[i].label = v[3];
					if (v.Length > 4)
						pkt[i].label += " " + v[4];
					//// er der ugyldige tegn i label?
					//if (strpos(label,"'") >-1) skrivfejl("Label må ikke indeholde tegnet '");
				}

				// kortets hjørner
				// vi skal altid have lidt plads (25px) på de tre andre sider og 150px ekstra på højre side (plads til label)
				double xdif = ((xmax - xmin) / width) * 25;
				xmin -= xdif;
				xmax += xdif * 6;

				double ydif = ((ymax - ymin) / height) * 25;
				ymin -= ydif;
				ymax += ydif;
//$xdif =($minwidth - $xmax + $xmin)/2;
//$ydif =($minwidth *($height/$width) - $xmax + $xmin)/2;

				// er koordinatforskellene store nok
				xdif = (minwidth - xmax + xmin) / 2;
				ydif = (minwidth * (height / width) - ymax + ymin) / 2;
				if (xdif > 0)
				{
					xmax += xdif;
					xmin -= xdif;
				}
				if (ydif > 0)
				{
					ymax += ydif;
					ymin -= ydif;
				}
				// og så skal vi sikre at billedets ratio holder
				double xpix =(xmax - xmin) /width;
				double ypix =(ymax - ymin) /height;

				if (xpix > ypix) // flere km pr vandret pixel end pr lodret - den lodrette skal justeres op
				{
					ydif = (xpix - ypix)*height/2;
					ymax += ydif;
					ymin -= ydif;
				}
				else if (xpix < ypix)
				{
					xdif = (ypix - xpix) * width / 2;
					xmax += xdif;
					xmin -= xdif;

				}
					
				// omregning til billed koordinater
				double faktor = width / (xmax - xmin);
				// hent baggrundskort
				string mapURLrå = "http://kortforsyningen.kms.dk/service?LAYERS=dtk_skaermkort&SERVICENAME=topo_skaermkort&VERSION=1.1.1" +
								 "&REQUEST=GetMap&SRS=EPSG%3A25832&FORMAT=image%2Fjpeg&SERVICE=wms&login=le342&password=qazwsx23" +
								 "&TILED=false&STYLES=&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&BBOX=" + xmin + "," + ymin + "," + xmax + "," + ymax +
								 "&WIDTH=" + width + "&HEIGHT=" + height;
				/*
				 * width=570&height=570&minwidth=1500&points=
					545748  6103069  A  99079;
				 * 495131  6094533  A  98789;
				 * 507169  6120318  A  99047;
				 * 507169  6120318  A  99048
				*/
					//mapext=515330.263158  6083279.16667  554658.421053  6133519.83333
				

				try
				{
					WebClient wc = new WebClient();
					byte[] bytes = wc.DownloadData(mapURLrå);
					MemoryStream ms = new MemoryStream(bytes);
					img = Image.FromStream(ms);
					Graphics g = Graphics.FromImage(img);
					//SolidBrush p = new SolidBrush(Color.Black);
					g.SmoothingMode = SmoothingMode.AntiAlias;
					g.InterpolationMode = InterpolationMode.HighQualityBicubic;
					g.PixelOffsetMode = PixelOffsetMode.HighQuality;

					g.Flush();
					// tegn punkterne
					for (int i = 0; i < li.Length; i++)
					{
						int x = (int)Math.Round((pkt[i].x - xmin) * faktor, 0);
						int y = height - (int)Math.Round((pkt[i].y - ymin) * faktor, 0);
						//SolidBrush b = new SolidBrush(Color.Black);
						//if (pkt[i].farve == 0)
						//{
						//	g.f.FillEllipse(new SolidBrush(frv[pkt[i].farve]), x - 11, y - 11, 23, 23);

						//}
						//else
						g.FillEllipse(new SolidBrush(frv[pkt[i].farve]), x - 11, y - 11, 23, 23);
						g.DrawString(pkt[i].bogstav + "  " + pkt[i].label, new Font("Verdana", 15), Brushes.Black, new RectangleF(x -10, y - 13, 120, 23));

					}
					g.Flush();

				}
				catch (Exception e)
				{
					throw new Exception("imgfejl " + e.Message);
				}

			}
			catch (Exception ex)
			{
				if (width == 0 || height == 0)
				{
					width = 500;
					height = 500;
				}
				img = new Bitmap(width, height);
				Graphics g = Graphics.FromImage(img);
				g.DrawString(ex.Message, new Font("Tahoma", 24), Brushes.Black, new RectangleF(10, 10, width - 20, height - 20));
				g.Flush();
			
			}
			using (MemoryStream mem = new MemoryStream())
			{
				img.Save(mem, ImageFormat.Png);
				mem.Seek(0, SeekOrigin.Begin);

				context.Response.ContentType = "image/png";

				mem.CopyTo(context.Response.OutputStream, 4096);
				context.Response.Flush();
			}
		}
	
		public bool IsReusable
		{
			get
			{
				return false;
			}
		}

		//public Image skrivfejl(string txt)
		//{
		//	if (width == 0 || height == 0)
		//	{
		//		width = 500;
		//		height = 500;
		//	}
		//	System.Drawing.Image img = new Bitmap(width, height);
		//	Graphics g = Graphics.FromImage(img);
		//	g.DrawString(txt, new Font("Tahoma", 8), Brushes.Black, new RectangleF(10, 10, width - 20, height -20));
		//	g.Flush();
		//	return img;
		//	//using (MemoryStream mem = new MemoryStream())
		//	//{
		//	//	img.Save(mem, ImageFormat.Png);
		//	//	mem.Seek(0, SeekOrigin.Begin);

		//	//	context.Response.ContentType = "image/png";

		//	//	mem.CopyTo(context.Response.OutputStream, 4096);
		//	//	context.Response.Flush();
		//	//}
		//}

	}
}